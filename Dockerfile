EXPOSE 80/tcp

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN pecl install xdebug && echo "zend_extension=$(find /usr/lib/php/ -name xdebug.so)" > /etc/php/7.1/mods-available/xdebug.ini && echo "xdebug.remote_enable=1" >> /etc/php/7.1/mods-available/xdebug.ini && echo "xdebug.remote_handler=dbgp" >> /etc/php/7.1/mods-available/xdebug.ini && echo "xdebug.remote_port=9000" >> /etc/php/7.1/mods-available/xdebug.ini && echo "xdebug.remote_autostart=1" >> /etc/php/7.1/mods-available/xdebug.ini && echo "xdebug.remote_connect_back=1" >> /etc/php/7.1/mods-available/xdebug.ini && echo "xdebug.idekey=debugit" >> /etc/php/7.1/mods-available/xdebug.ini && ln -s /etc/php/7.1/mods-available/xdebug.ini /etc/php/7.1/cli/conf.d/20-xdebug.ini && ln -s /etc/php/7.1/mods-available/xdebug.ini /etc/php/7.1/fpm/conf.d/20-xdebug.ini

RUN php -r "readfile('https://getcomposer.org/installer');" | php && mv composer.phar /usr/local/bin/composer

RUN chmod +x /etc/service/phpfpm/run

ADD file:f212573361a7c56985a6a95f3457b137d370200605cba3201e0be36d84782a54 in /etc/service/phpfpm/run

RUN mkdir /etc/service/phpfpm

RUN chmod +x /etc/service/nginx/run

COPY file:6dcfd928c4696044ca38456b944612e03b1557a55bb948cbf459de7f4f519a0e in /etc/service/nginx/run

RUN mkdir /etc/service/nginx

ADD file:65ee4f1df7d03b1f324fe6c05414507fa0bb2b1f0208a36109b267345e203616 in /server/http/public/index.php

RUN sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/7.1/fpm/php.ini

RUN sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.1/fpm/php-fpm.conf

RUN sed -i "s/;date.timezone =.*/date.timezone = UTC/" /etc/php/7.1/cli/php.ini

RUN sed -i "s/;date.timezone =.*/date.timezone = UTC/" /etc/php/7.1/fpm/php.ini

RUN mkdir -p /usr/local/openssl/include/openssl/ && ln -s /usr/include/openssl/evp.h /usr/local/openssl/include/openssl/evp.h && mkdir -p /usr/local/openssl/lib/ && ln -s /usr/lib/x86_64-linux-gnu/libssl.a /usr/local/openssl/lib/libssl.a && ln -s /usr/lib/x86_64-linux-gnu/libssl.so /usr/local/openssl/lib/

RUN apt-get update && apt-get install -y libapache2-mod-php7.1 php7.1 php7.1-fpm php7.1-cli php7.1-common libpcre3-dev php7.1-dev php7.1-gd php7.1-curl php7.1-mcrypt php7.1-intl php7.1-mysql php7.1-pgsql php7.1-mbstring php7.1-json php7.1-opcache php7.1-xml php7.1-odbc php7.1-zip php7.1-bcmath php-pear libsasl2-dev

RUN LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php

RUN apt-get install -y language-pack-en-base

ADD file:2c3af4af1f7fb0027b776c5d65a9e85b92cd56f4fccbd6da511544f3bb3c7049 in /etc/nginx/sites-available/default

ADD file:5f6acf4f9f2eec76945129a625634860471a94781ba390dfc9857eb6b1951d9f in /etc/nginx/nginx.conf

RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y nginx

RUN apt-get update

RUN add-apt-repository -y ppa:nginx/stable

RUN apt-get install -y software-properties-common python-software-properties wget curl vim htop git

RUN apt-get update

CMD ["/sbin/my_init"]

RUN /etc/my_init.d/00_regen_ssh_host_keys.sh

ENV LC_ALL=en_US.UTF-8

ENV LANG=en_US.UTF-8

RUN locale-gen en_US.UTF-8

ENV HOME=/root

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y -o Dpkg::Options::="--force-confold"

ONBUILD RUN apt-get update && apt-get upgrade -y -o Dpkg::Options::="--force-confold"

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get update && apt-get install -y python && apt-get upgrade -y -o Dpkg::Options::="--force-confold" # break that cache

ENV DEBIAN_FRONTEND=noninteractive

CMD ["/sbin/my_init"]

CMD ["/sbin/my_init"]

RUN /bd_build/prepare.sh && /bd_build/system_services.sh && /bd_build/utilities.sh && /bd_build/cleanup.sh

ADD dir:d6bef9db55afddb775420fc2166b0d55a06dda28aa545f8c9fd51c9bdfa5705c in /bd_build

MAINTAINER Phusion <info@phusion.nl>

CMD ["/bin/bash"]

RUN sed -i 's/^#\s*\(deb.*universe\)$/\1/g' /etc/apt/sources.list

RUN echo '#!/bin/sh' > /usr/sbin/policy-rc.d && echo 'exit 101' >> /usr/sbin/policy-rc.d && chmod +x /usr/sbin/policy-rc.d && dpkg-divert --local --rename --add /sbin/initctl && cp -a /usr/sbin/policy-rc.d /sbin/initctl && sed -i 's/^exit.*/exit 0/' /sbin/initctl && echo 'force-unsafe-io' > /etc/dpkg/dpkg.cfg.d/docker-apt-speedup && echo 'DPkg::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' > /etc/apt/apt.conf.d/docker-clean && echo 'APT::Update::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' >> /etc/apt/apt.conf.d/docker-clean && echo 'Dir::Cache::pkgcache ""; Dir::Cache::srcpkgcache "";' >> /etc/apt/apt.conf.d/docker-clean && echo 'Acquire::Languages "none";' > /etc/apt/apt.conf.d/docker-no-languages && echo 'Acquire::GzipIndexes "true"; Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/docker-gzip-indexes

ADD file:b3447f4503091bb6bb6e5042ea05f41487c08d47e986b5892075e6015a4daae0 in /